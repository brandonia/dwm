dwm
---

Welcome to my own personal build of dwm! Feel free to try it out if you want.

### Changes
- Set modkey to Super key
- Added some of my own keybindings
- Set some window rules for multi monitors
- Colors!
- Changed up tag labels


#### Keybindings

| Binding | Action |
| :--- | :--- |
|`mod + w` | Firefox |
|`mod + s` | Steam |
|`mod + v` | Discord |
|`mod + e` | Code Editor |
|`mod + f` | File manager |
| `PrtSc`  | flameshot full |
| `Shift + PrtSc` | flameshot gui |

#### Patches

- uselessgaps
- restartsig
- layouts ( fibonacci, three column )
- colorbar
- systray
